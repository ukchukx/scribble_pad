

package draw1; 

import java.util.EventListener;

import scribble3.*;

@SuppressWarnings("serial")
public class DrawingCanvas extends ScribbleCanvas {

  public DrawingCanvas() {
  }

  public void setTool(Tool tool) { 
    drawingCanvasListener.setTool(tool);
  }

  public Tool getTool() { 
    return drawingCanvasListener.getTool(); 
  }

  // factory method 
  protected EventListener makeCanvasListener() {
    return (drawingCanvasListener = new DrawingCanvasListener(this)); 
  }

  protected DrawingCanvasListener drawingCanvasListener; 

}
